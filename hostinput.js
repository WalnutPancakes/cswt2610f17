var http = require('http');
var formidable = require("formidable");
var fs = require('fs');
var filepath;
var testFolder = './tests/';
var url = require('url');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': "text/plain"});
  res.end('Hello World');
  
  var file = url.parse(req.url).pathname;
  fs.readFile(file, function(err, data) {
	res.write(data)
  });
  
}).listen(8080);