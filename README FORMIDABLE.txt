to install:
npm install -g formidable


Add an environment variable called NODE_PATH and set it to %USERPROFILE%\Application Data\npm\node_modules
 (Windows XP), %AppData%\npm\node_modules (Windows 7/8/10), or wherever npm ends up installing the modules 
 on your Windows flavor. To be done with it once and for all, add this as a System variable in the Advanced
 tab of the System Properties dialog (run control.exe sysdm.cpl,System,3).

Quick solution in Windows 7+ is to just run:

rem for future
setx NODE_PATH %AppData%\npm\node_modules
rem for current session
set NODE_PATH=%AppData%\npm\node_modules
