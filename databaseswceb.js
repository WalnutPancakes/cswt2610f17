var mysql = require('mysql')

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query("CREATE DATABASE wceb", function (err, result) {
    if (err) throw err;
	console.log("Database created");
  });
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "CREATE TABLE hosts (id INT AUTO_INCREMENT PRIMARY KEY, hostname VARCHAR(255), showname VARCHAR(255))";
  con.query(sql, function (err, result) {
    if (err) throw err;
	console.log("Host Table Created");
  });
});

