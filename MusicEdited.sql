DROP DATABASE IF EXISTS music;
CREATE DATABASE music;
USE music;

CREATE TABLE album (
  artist_id SMALLINT(5) NOT NULL DEFAULT 0,
  album_id SMALLINT(4) NOT NULL DEFAULT 0,
  album_name CHAR(128) DEFAULT NULL,
  PRIMARY KEY  (artist_id,album_id)
  
);

CREATE TABLE artist (
  artist_id SMALLINT(5) NOT NULL DEFAULT 0,
  artist_name CHAR(128) DEFAULT NULL,
  PRIMARY KEY  (artist_id)
  
);

CREATE TABLE genre (
  artist_id SMALLINT(5) NOT NULL DEFAULT 0,
  album_id SMALLINT(4) NOT NULL DEFAULT 0,
  genre VARCHAR(45) NOT NULL,
  genre_id SMALLINT(5) NOT NULL,
  PRIMARY KEY  (artist_id,album_id,genre_id)
);

CREATE TABLE track (
  track_id SMALLINT(3) NOT NULL DEFAULT 0,
  track_name CHAR(128) DEFAULT NULL,
  artist_id SMALLINT(5) NOT NULL DEFAULT 0,
  album_id SMALLINT(4) NOT NULL DEFAULT 0,
  time DECIMAL(5,2) DEFAULT NULL,
  PRIMARY KEY  (artist_id,album_id,track_id)
);


INSERT INTO artist VALUES (1, "Led Zeppelin");
INSERT INTO artist VALUES (2, "Dr Dre");
INSERT INTO artist VALUES (3, "Rolling Stones");
INSERT INTO artist VALUES (4, "Snoop Dogg");
INSERT INTO artist VALUES (5, "Childish Gambino");
INSERT INTO artist VALUES (6, "Wu Tang Clan");
INSERT INTO artist VALUES (7, "Garth Brooks");
INSERT INTO artist VALUES (8, "3 Doors Down");
INSERT INTO artist VALUES (9, "Michael Jackson");
INSERT INTO artist VALUES (10, "Daft Punk");
INSERT INTO artist VALUES (11, "John Mayer");

INSERT INTO album VALUES (1, 01, "Led Zeppelin III");
INSERT INTO album VALUES (2, 02, "The Chronic");
INSERT INTO album VALUES (3, 03, "Let It Bleed");
INSERT INTO album VALUES (4,04, "Doggystle");
INSERT INTO album VALUES (5,05, "Awaken, My Love");
INSERT INTO album VALUES (6,06, "Enter the Wu-Tang (36 Chambers)");
INSERT INTO album VALUES (7,07, "The Chase");
INSERT INTO album VALUES (8,08, "3 Doors Down");
INSERT INTO album VALUES (9,09, "Bad");
INSERT INTO album VALUES (10,10, "Random Access Memories");
INSERT INTO album VALUES (11,11, "Room For Squares");

INSERT INTO track VALUES (1101, "No Such Thing", 11, 11, 3.51);
INSERT INTO track VALUES (1102,"Why Georgia",11,11,4.30);
INSERT INTO track VALUES (1103,"My Stupid Mouth",11,11,3.47);
INSERT INTO track VALUES (1104,"Your Body Is a Wonderland",11,11,4.08);
INSERT INTO track VALUES (1105,"Neon",11,11,4.23);
INSERT INTO track VALUES (1106,"City Love",11,11,4.03);
INSERT INTO track VALUES (1107,"83",11,11,4.55);
INSERT INTO track VALUES (1108,"Love Song For No one",11,11,3.24);
INSERT INTO track VALUES (1109,"Back to You",11,11,4.04);
INSERT INTO track VALUES (1110,"Great Indoors",11,11,3.36);
INSERT INTO track VALUES (1111,"Not Myself",11,11,3.39);
INSERT INTO track VALUES (1112,"St. Patricks Day",11,11,5.20);
INSERT INTO track VALUES (113,"Immigrant Song",1,01,2.26);
INSERT INTO track VALUES (114,"Friends",1,01,3.55);
INSERT INTO track VALUES (115,"Celebration",1,01,3.29);
INSERT INTO track VALUES (116,"Since I've Been Loving You",1,01,3.29);
INSERT INTO track VALUES (117,"Out on the Tiles",1,01,4.04);
INSERT INTO track VALUES (118,"Gallows Pole",1,01,4.58);
INSERT INTO track VALUES (119,"Tangerine",1,01,3.12);
INSERT INTO track VALUES (120,"That's the Way",1,01,5.38);
INSERT INTO track VALUES (121,"Bron-Y-Aur",1,01,4.20);
INSERT INTO track VALUES (122,"Hats Off to Roy Harper",1,01,3.41);
INSERT INTO track VALUES (23,"We Shall Be Free",7,07,3.48);
INSERT INTO track VALUES (24,"Somewhere Other Than The Night",7,07,3.11);
INSERT INTO track VALUES (725,"Mr Right",7,07,2.01);
INSERT INTO track VALUES (726,"Every Now and Then",7,07,4.16);
INSERT INTO track VALUES (727,"Walkin After Midnight",7,07,2.33);
INSERT INTO track VALUES (728,"Dixie Chicken",7,07,4.25);
INSERT INTO track VALUES (729,"Learning to Live Again",7,07,4.06);
INSERT INTO track VALUES (730,"That Summer",7,07,4.47);
INSERT INTO track VALUES (731,"Something With a Ring To It",7,07,2.33);
INSERT INTO track VALUES (732,"Night Riders Lament",7,07,4.05);
INSERT INTO track VALUES (733,"Face to Face",7,07,4.26);
INSERT INTO track VALUES (1001,"Give Life Back to Music",10,10,4.34);
INSERT INTO track VALUES (1002,"The Game of Love",10,10,5.21);
INSERT INTO track VALUES (1003,"Giorgio By Moroder",10,10,9.04);
INSERT INTO track VALUES (1004,"Within",10,10,3.48);
INSERT INTO track VALUES (1005,"Instant Crush",10,10,5.37);
INSERT INTO track VALUES (1006,"Lose Yourself to Dance",10,10,5.53);
INSERT INTO track VALUES (1007,"Touch",10,10,8.18);
INSERT INTO track VALUES (1008,"Get Lucky",10,10,6.08);
INSERT INTO track VALUES (1009,"Beyond",10,10,4.50);
INSERT INTO track VALUES (1010,"Motherboard",10,10,5.41);
INSERT INTO track VALUES (1011,"Fragments of Time",10,10,4.39);
INSERT INTO track VALUES (1012,"Doin it Right",10,10,4.11);
INSERT INTO track VALUES (1013,"Contact",10,10,6.21);
INSERT INTO track VALUES (801,"Train",8,08,3.10);
INSERT INTO track VALUES (802,"Citizen/Soldier",8,08,3.52);
INSERT INTO track VALUES (803,"It's Not My Time",8,08,4.01);
INSERT INTO track VALUES (804,"Let Me Be Myself",8,08,3.48);
INSERT INTO track VALUES (805,"Pages",8,08,3.47);
INSERT INTO track VALUES (806,"It's The Only One You've Got",8,08,4.23);
INSERT INTO track VALUES (807,"Give It to Me",8,08,3.21);
INSERT INTO track VALUES (808,"These Days",8,08,3.39);
INSERT INTO track VALUES (809,"Your Arms Feel Like Home",8,08,3.44);
INSERT INTO track VALUES (810,"Runaway",8,08,3.24);
INSERT INTO track VALUES (811,"When It's Over",8,08,4.18);
INSERT INTO track VALUES (812,"She Dont Want the World",8,08,4.03);
INSERT INTO track VALUES (301,"Gimme Shelter",3,03,4.31);
INSERT INTO track VALUES (302,"Love in Vain",3,03,4.19);
INSERT INTO track VALUES (303,"Country Honk",3,03,3.09);
INSERT INTO track VALUES (304,"Live with Me",3,03,3.33);
INSERT INTO track VALUES (305,"Let it Bleed",3,03,5.26);
INSERT INTO track VALUES (306,"Midnight Rambler",3,03,6.52);
INSERT INTO track VALUES (307,"You Got the Silver",3,03,2.51);
INSERT INTO track VALUES (308,"Monkey Man",3,03,4.12);
INSERT INTO track VALUES (309,"You Can't Always Get What You Want",3,03,7.28);
INSERT INTO track VALUES (901,"Bad",9,09,4.08);
INSERT INTO track VALUES (902,"The Way You Make Me Feel",9,09,4.59);
INSERT INTO track VALUES (903,"Speed Demon",9,09,4.03);
INSERT INTO track VALUES (904,"Liberian Girl",9,09,3.55);
INSERT INTO track VALUES (905,"Just Good Friends",9,09,4.09);
INSERT INTO track VALUES (906,"Another Part of Me",9,09,3.55);
INSERT INTO track VALUES (907,"Man in the Mirror",9,09,5.21);
INSERT INTO track VALUES (908,"I Just Can't Stop Loving You",9,09,4.27);
INSERT INTO track VALUES (909,"Dirty Diana",9,09,4.42);
INSERT INTO track VALUES (910,"Smooth Criminal",9,09,4.20);
INSERT INTO track VALUES (501, "Me and Your Mama",5,05,6.19);
INSERT INTO track VALUES (502, "Have Some Love",5,05,3.44);
INSERT INTO track VALUES (503, "Boogieman",5,05,3.36);
INSERT INTO track VALUES (504, "Zombies",5,05,4.41);
INSERT INTO track VALUES (505, "Riot",5,05,2.05);
INSERT INTO track VALUES (506, "Redbone",5,05,5.26);
INSERT INTO track VALUES (507, "California",5,05,2.45);
INSERT INTO track VALUES (508, "Terrified",5,05,4.15);
INSERT INTO track VALUES (509, "Baby Boy",5,05,6.22);
INSERT INTO track VALUES (510, "The Night Me and Your Mama Met",5,05,3.34);
INSERT INTO track VALUES (511, "Stand Tall",5,05,6.10);
